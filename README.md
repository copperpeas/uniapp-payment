# uniapp-payment

#### 介绍
uniapp支付

#### 微信支付流程

测试接入的是uniapp官方预下单接口

APP应用首先去微信等开发平台申请开通支付，部分支付渠道需要配置支付目录，授权域名，回调函数

预下单返回参考：
``` JS
"data": {
    "appid": "wx0411fa6a39d61297",
    "noncestr": "Xu70l0bOQSQIDIPH",
    "package": "Sign=WXPay",
    "partnerid": "1230636401",
    "prepayid": "wx271034552844601252843eae1384334800",
    "timestamp": 1569551695,
    "sign": "44B127098800419D542B688054F95A8F"
  },
```

参考官方微信支付 https://pay.weixin.qq.com/wiki/doc/api/index.html 


小程序支付：https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=7_3&index=1

简单流程：
第一步：调用登录接口，获取到用户的openid等
第二步：Code，价格等发送给后台，后台发送微信生成预支付订单，并且返回订单信息
第三步：小程序拿到预下单订单信息，发起支付，调起支付APP
第四步：后台验证支付结果

小程序第三步调起支付给APP调起支付参数不一样，大体流程是一致的


商户系统和微信支付系统主要交互：

1、小程序内调用登录接口，获取到用户的openid,api参见公共api【小程序登录API】

2、商户server调用支付统一下单，api参见公共api【统一下单API】

3、商户server调用再次签名，api参见公共api【再次签名】

4、商户server接收支付通知，api参见公共api【支付结果通知API】

5、商户server查询支付结果，api参见公共api【查询订单API】

拿到预下单后调用支付
``` JS
uni.requestPayment({
    timeStamp: paymentData.timeStamp,
    nonceStr: paymentData.nonceStr,
    package: paymentData.package,
    signType: 'MD5',
    paySign: paymentData.paySign,
    success: (res) => {
        uni.showToast({
            title: "感谢您的赞助!"
        })
    },
    fail: (res) => {
        uni.showModal({
            content: "支付失败,原因为: " + res
                .errMsg,
            showCancel: false
        })
    },
    complete: () => {
        this.loading = false;
    }
})
```


APP支付 https://pay.weixin.qq.com/wiki/doc/api/app/app.php?chapter=8_3

简单流程：

第一步：获取支付类型，APPid，支付金额
第二步：发送给后台，后台发送微信生成预支付订单，并且返回订单信息
第三步：拿到预下单订单信息，发起支付，调起支付APP
第四步：后台验证支付结果
           
商户系统和微信支付系统主要交互说明：

步骤1：用户在商户APP中选择商品，提交订单，选择微信支付。

步骤2：商户后台收到用户支付单，调用微信支付统一下单接口。参见【统一下单API】。

步骤3：统一下单接口返回正常的prepay_id，再按签名规范重新生成签名后，将数据传输给APP。参与签名的字段名为appid，partnerid，prepayid，noncestr，timestamp，package。注意：package的值格式为Sign=WXPay

步骤4：商户APP调起微信支付。api参见本章节【app端开发步骤说明】

步骤5：商户后台接收支付通知。api参见【支付结果通知API】

步骤6：商户后台查询支付结果。api参见【查询订单API】

拿到预下单后调用支付
``` JS
uni.requestPayment({
    provider: e.id,
    orderInfo: orderInfo.data,
    success: (e) => {
        console.log("success", e);
        uni.showToast({
            title: "感谢您的赞助!"
        })
    },
    fail: (e) => {
        console.log("fail", e);
        uni.showModal({
            content: "支付失败,原因为: " + e.errMsg,
            showCancel: false
        })
    },
    complete: () => {
        this.providerList[index].loading = false;
    }
})
```


微信有预下单，而支付宝不存在



#### 支付宝支付流程
支付宝支付参考  https://docs.open.alipay.com/  左边支付栏目
支付宝APP支付  https://docs.open.alipay.com/204/105297

简单流程：  
第一步：获取支付类型，APPid，支付金额  
第二步：发送给后台，后台针对支付宝请求参数说明，组装数据，返回订单信息  
第三步：拿到组装单订单信息，发起支付，调起支付APP  
第四步：后台验证支付结果  

<img src="./zfb.png" width="786" height="270" alt="演示图片"/>


